package com.example.tech_demo.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002:\u0001\fB\u0017\u0012\b\u0010\u0003\u001a\u0004\u0018\u00018\u0000\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0015\u0010\u0003\u001a\u0004\u0018\u00018\u0000\u00a2\u0006\n\n\u0002\u0010\t\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\r"}, d2 = {"Lcom/example/tech_demo/data/Result;", "T", "", "data", "networkState", "Lcom/example/tech_demo/data/Result$NetworkState;", "(Ljava/lang/Object;Lcom/example/tech_demo/data/Result$NetworkState;)V", "getData", "()Ljava/lang/Object;", "Ljava/lang/Object;", "getNetworkState", "()Lcom/example/tech_demo/data/Result$NetworkState;", "NetworkState", "app_debug"})
public final class Result<T extends java.lang.Object> {
    @org.jetbrains.annotations.Nullable()
    private final T data = null;
    @org.jetbrains.annotations.NotNull()
    private final com.example.tech_demo.data.Result.NetworkState networkState = null;
    
    public Result(@org.jetbrains.annotations.Nullable()
    T data, @org.jetbrains.annotations.NotNull()
    com.example.tech_demo.data.Result.NetworkState networkState) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final T getData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.tech_demo.data.Result.NetworkState getNetworkState() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005\u00a8\u0006\u0006"}, d2 = {"Lcom/example/tech_demo/data/Result$NetworkState;", "", "(Ljava/lang/String;I)V", "LOADING", "ERROR", "SUCCESS", "app_debug"})
    public static enum NetworkState {
        /*public static final*/ LOADING /* = new LOADING() */,
        /*public static final*/ ERROR /* = new ERROR() */,
        /*public static final*/ SUCCESS /* = new SUCCESS() */;
        
        NetworkState() {
        }
    }
}
package com.example.tech_demo.ui.movies;

import java.lang.System;

@org.koin.core.component.KoinApiExtension()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u000b\u001a\u00020\fH\u0002J\u0012\u0010\r\u001a\u00020\f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\u0010"}, d2 = {"Lcom/example/tech_demo/ui/movies/MoviesActivity;", "Lcom/example/tech_demo/ui/movies/BaseActivity;", "()V", "moviesAdapter", "Lcom/example/tech_demo/ui/movies/MoviesAdapter;", "moviesViewModel", "Lcom/example/tech_demo/data/MoviesViewModel;", "getMoviesViewModel", "()Lcom/example/tech_demo/data/MoviesViewModel;", "moviesViewModel$delegate", "Lkotlin/Lazy;", "fetchMovies", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_debug"})
public final class MoviesActivity extends com.example.tech_demo.ui.movies.BaseActivity {
    private final kotlin.Lazy moviesViewModel$delegate = null;
    private final com.example.tech_demo.ui.movies.MoviesAdapter moviesAdapter = null;
    private java.util.HashMap _$_findViewCache;
    
    public MoviesActivity() {
        super();
    }
    
    private final com.example.tech_demo.data.MoviesViewModel getMoviesViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void fetchMovies() {
    }
}
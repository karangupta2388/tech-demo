package com.example.tech_demo.ui.movies;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016\u00a8\u0006\t"}, d2 = {"Lcom/example/tech_demo/ui/movies/MoviesAdapter;", "Lcom/example/tech_demo/ui/movies/BaseAdapter;", "()V", "getLayoutId", "", "getViewHolder", "Lcom/example/tech_demo/ui/movies/ViewHolderPresenter;", "binding", "Landroidx/databinding/ViewDataBinding;", "app_debug"})
public final class MoviesAdapter extends com.example.tech_demo.ui.movies.BaseAdapter {
    
    public MoviesAdapter() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.example.tech_demo.ui.movies.ViewHolderPresenter getViewHolder(@org.jetbrains.annotations.NotNull()
    androidx.databinding.ViewDataBinding binding) {
        return null;
    }
    
    @java.lang.Override()
    public int getLayoutId() {
        return 0;
    }
}
package com.example.tech_demo.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\n\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u00070\u000bR \u0010\u0005\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/example/tech_demo/data/MoviesViewModel;", "Lcom/example/tech_demo/data/BaseViewModel;", "moviesRepository", "Lcom/example/tech_demo/data/repo/MoviesRepository;", "(Lcom/example/tech_demo/data/repo/MoviesRepository;)V", "moviesLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lcom/example/tech_demo/data/Result;", "", "Lcom/example/tech_demo/data/model/MoviesModel;", "fetchMovies", "Landroidx/lifecycle/LiveData;", "app_debug"})
public final class MoviesViewModel extends com.example.tech_demo.data.BaseViewModel {
    private final com.example.tech_demo.data.repo.MoviesRepository moviesRepository = null;
    private final androidx.lifecycle.MutableLiveData<com.example.tech_demo.data.Result<java.util.List<com.example.tech_demo.data.model.MoviesModel>>> moviesLiveData = null;
    
    public MoviesViewModel(@org.jetbrains.annotations.NotNull()
    com.example.tech_demo.data.repo.MoviesRepository moviesRepository) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.example.tech_demo.data.Result<java.util.List<com.example.tech_demo.data.model.MoviesModel>>> fetchMovies() {
        return null;
    }
}
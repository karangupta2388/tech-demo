package com.example.tech_demo.ui.movies;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u00020\u0006H\u0014J\n\u0010\u0007\u001a\u00020\u0006*\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/example/tech_demo/ui/movies/BaseActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "disposables", "Lio/reactivex/disposables/CompositeDisposable;", "onDestroy", "", "clearOnDestroy", "Lio/reactivex/disposables/Disposable;", "app_debug"})
public class BaseActivity extends androidx.appcompat.app.AppCompatActivity {
    private final io.reactivex.disposables.CompositeDisposable disposables = null;
    private java.util.HashMap _$_findViewCache;
    
    public BaseActivity() {
        super();
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    public final void clearOnDestroy(@org.jetbrains.annotations.NotNull()
    io.reactivex.disposables.Disposable $this$clearOnDestroy) {
    }
}
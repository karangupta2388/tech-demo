package com.example.tech_demo.data.repo;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0016\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/example/tech_demo/data/repo/MoviesRepository;", "", "moviesApiService", "Lcom/example/tech_demo/data/MoviesAPIService;", "(Lcom/example/tech_demo/data/MoviesAPIService;)V", "fetchMovies", "Lio/reactivex/Single;", "Lcom/example/tech_demo/data/model/MoviesResponseModel;", "app_debug"})
public class MoviesRepository {
    private final com.example.tech_demo.data.MoviesAPIService moviesApiService = null;
    
    public MoviesRepository(@org.jetbrains.annotations.NotNull()
    com.example.tech_demo.data.MoviesAPIService moviesApiService) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.reactivex.Single<com.example.tech_demo.data.model.MoviesResponseModel> fetchMovies() {
        return null;
    }
}
package com.example.tech_demo.ui.camera;

import java.lang.System;

@org.koin.core.component.KoinApiExtension()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u00a0\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&H\u0016J\b\u0010\'\u001a\u00020$H\u0002J\u0012\u0010(\u001a\u0004\u0018\u00010\u00122\u0006\u0010)\u001a\u00020*H\u0002J\b\u0010+\u001a\u00020,H\u0002J\u0010\u0010-\u001a\u0004\u0018\u00010.2\u0006\u0010/\u001a\u00020\u0012J\b\u00100\u001a\u00020$H\u0016J\u0010\u00101\u001a\u00020$2\u0006\u00102\u001a\u000203H\u0017J\u0012\u00104\u001a\u00020$2\b\u00105\u001a\u0004\u0018\u000106H\u0014J\b\u00107\u001a\u00020$H\u0014J\b\u00108\u001a\u00020$H\u0007J\u0018\u00109\u001a\u0004\u0018\u00010\t2\u0006\u0010:\u001a\u00020\t2\u0006\u0010;\u001a\u00020<J\u001a\u00109\u001a\u0004\u0018\u00010\t2\u0006\u0010=\u001a\u00020>2\u0006\u0010\b\u001a\u00020\tH\u0002J\u0010\u0010?\u001a\u00020$2\u0006\u0010@\u001a\u00020\fH\u0007R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u001dX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001f\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010!\u001a\u0004\u0018\u00010\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006A"}, d2 = {"Lcom/example/tech_demo/ui/camera/CameraPreviewActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Landroidx/camera/core/ImageAnalysis$Analyzer;", "Landroid/view/View$OnClickListener;", "()V", "bCapture", "Landroid/widget/Button;", "bRecord", "bitmap", "Landroid/graphics/Bitmap;", "cameraProviderFuture", "Lcom/google/common/util/concurrent/ListenableFuture;", "Landroidx/camera/lifecycle/ProcessCameraProvider;", "compositeDisposable", "Lio/reactivex/disposables/CompositeDisposable;", "imageCapture", "Landroidx/camera/core/ImageCapture;", "mediaStorageDir", "Ljava/io/File;", "getMediaStorageDir", "()Ljava/io/File;", "setMediaStorageDir", "(Ljava/io/File;)V", "ok", "previewImage", "Landroid/widget/ImageView;", "previewImageContainer", "Landroid/widget/FrameLayout;", "previewView", "Landroidx/camera/view/PreviewView;", "retry", "uri", "Landroid/net/Uri;", "videoCapture", "Landroidx/camera/core/VideoCapture;", "analyze", "", "image", "Landroidx/camera/core/ImageProxy;", "capturePhoto", "createImageFilePath", "extraTimeStamp", "", "getExecutor", "Ljava/util/concurrent/Executor;", "getImageSize", "", "imageFilePath", "onBackPressed", "onClick", "view", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "recordVideo", "rotateImage", "source", "angle", "", "photoPath", "", "startCameraX", "cameraProvider", "app_debug"})
public final class CameraPreviewActivity extends androidx.appcompat.app.AppCompatActivity implements androidx.camera.core.ImageAnalysis.Analyzer, android.view.View.OnClickListener {
    private com.google.common.util.concurrent.ListenableFuture<androidx.camera.lifecycle.ProcessCameraProvider> cameraProviderFuture;
    private androidx.camera.view.PreviewView previewView;
    private android.widget.FrameLayout previewImageContainer;
    private android.widget.ImageView previewImage;
    private androidx.camera.core.ImageCapture imageCapture;
    private androidx.camera.core.VideoCapture videoCapture;
    private android.widget.Button bRecord;
    private android.widget.Button bCapture;
    private android.widget.Button retry;
    private android.widget.Button ok;
    private android.net.Uri uri;
    private android.graphics.Bitmap bitmap;
    private io.reactivex.disposables.CompositeDisposable compositeDisposable;
    @org.jetbrains.annotations.Nullable()
    private java.io.File mediaStorageDir;
    private java.util.HashMap _$_findViewCache;
    
    public CameraPreviewActivity() {
        super();
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final java.util.concurrent.Executor getExecutor() {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"RestrictedApi"})
    public final void startCameraX(@org.jetbrains.annotations.NotNull()
    androidx.camera.lifecycle.ProcessCameraProvider cameraProvider) {
    }
    
    @java.lang.Override()
    public void analyze(@org.jetbrains.annotations.NotNull()
    androidx.camera.core.ImageProxy image) {
    }
    
    @android.annotation.SuppressLint(value = {"RestrictedApi"})
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    @android.annotation.SuppressLint(value = {"RestrictedApi"})
    public final void recordVideo() {
    }
    
    private final void capturePhoto() {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final int[] getImageSize(@org.jetbrains.annotations.NotNull()
    java.io.File imageFilePath) {
        return null;
    }
    
    private final android.graphics.Bitmap rotateImage(java.lang.String photoPath, android.graphics.Bitmap bitmap) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.graphics.Bitmap rotateImage(@org.jetbrains.annotations.NotNull()
    android.graphics.Bitmap source, float angle) {
        return null;
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.io.File getMediaStorageDir() {
        return null;
    }
    
    public final void setMediaStorageDir(@org.jetbrains.annotations.Nullable()
    java.io.File p0) {
    }
    
    private final java.io.File createImageFilePath(int extraTimeStamp) {
        return null;
    }
}
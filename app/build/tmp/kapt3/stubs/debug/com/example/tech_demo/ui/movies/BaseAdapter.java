package com.example.tech_demo.ui.movies;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\b&\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u0007\u001a\u00020\bH\u0016J\b\u0010\t\u001a\u00020\bH&J\u0010\u0010\n\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\fH&J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\bH\u0016J\u0018\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\bH\u0016J\u0014\u0010\u0015\u001a\u00020\u000e2\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Lcom/example/tech_demo/ui/movies/BaseAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/example/tech_demo/ui/movies/ViewHolderPresenter;", "()V", "dataList", "", "Lcom/example/tech_demo/data/model/Model;", "getItemCount", "", "getLayoutId", "getViewHolder", "binding", "Landroidx/databinding/ViewDataBinding;", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setValues", "app_debug"})
public abstract class BaseAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.example.tech_demo.ui.movies.ViewHolderPresenter> {
    private java.util.List<? extends com.example.tech_demo.data.model.Model> dataList;
    
    public BaseAdapter() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.example.tech_demo.ui.movies.ViewHolderPresenter onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.example.tech_demo.ui.movies.ViewHolderPresenter holder, int position) {
    }
    
    public final void setValues(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.example.tech_demo.data.model.Model> dataList) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.example.tech_demo.ui.movies.ViewHolderPresenter getViewHolder(@org.jetbrains.annotations.NotNull()
    androidx.databinding.ViewDataBinding binding);
    
    public abstract int getLayoutId();
}
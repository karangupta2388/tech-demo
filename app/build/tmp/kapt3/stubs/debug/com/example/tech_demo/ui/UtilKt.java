package com.example.tech_demo.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a \u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"setThrottledClickListener", "Lio/reactivex/disposables/Disposable;", "Landroid/view/View;", "throttlePeriod", "", "onNext", "Lio/reactivex/functions/Consumer;", "app_debug"})
public final class UtilKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final io.reactivex.disposables.Disposable setThrottledClickListener(@org.jetbrains.annotations.NotNull()
    android.view.View $this$setThrottledClickListener, long throttlePeriod, @org.jetbrains.annotations.NotNull()
    io.reactivex.functions.Consumer<android.view.View> onNext) {
        return null;
    }
}
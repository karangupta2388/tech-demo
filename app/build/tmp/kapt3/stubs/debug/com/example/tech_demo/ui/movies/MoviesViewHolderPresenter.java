package com.example.tech_demo.ui.movies;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016\u00a8\u0006\t"}, d2 = {"Lcom/example/tech_demo/ui/movies/MoviesViewHolderPresenter;", "Lcom/example/tech_demo/ui/movies/ViewHolderPresenter;", "binding", "Landroidx/databinding/ViewDataBinding;", "(Landroidx/databinding/ViewDataBinding;)V", "bind", "", "data", "Lcom/example/tech_demo/data/model/Model;", "app_debug"})
public final class MoviesViewHolderPresenter extends com.example.tech_demo.ui.movies.ViewHolderPresenter {
    
    public MoviesViewHolderPresenter(@org.jetbrains.annotations.NotNull()
    androidx.databinding.ViewDataBinding binding) {
        super(null);
    }
    
    @java.lang.Override()
    public void bind(@org.jetbrains.annotations.NotNull()
    com.example.tech_demo.data.model.Model data) {
    }
}
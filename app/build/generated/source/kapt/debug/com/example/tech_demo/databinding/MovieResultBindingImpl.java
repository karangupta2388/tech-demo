package com.example.tech_demo.databinding;
import com.example.tech_demo.R;
import com.example.tech_demo.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class MovieResultBindingImpl extends MovieResultBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.movie_poster, 4);
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public MovieResultBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private MovieResultBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[3]
            , (android.widget.ImageView) bindings[4]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[2]
            );
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.moviePopularity.setTag(null);
        this.movieTitle.setTag(null);
        this.movieVoteAverage.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.data == variableId) {
            setData((com.example.tech_demo.data.model.MoviesModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setData(@Nullable com.example.tech_demo.data.model.MoviesModel Data) {
        this.mData = Data;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.data);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        double dataVoteAverage = 0.0;
        java.lang.String stringValueOfDataPopularity = null;
        java.lang.String stringValueOfDataVoteAverage = null;
        double dataPopularity = 0.0;
        java.lang.String dataOriginalTitle = null;
        com.example.tech_demo.data.model.MoviesModel data = mData;

        if ((dirtyFlags & 0x3L) != 0) {



                if (data != null) {
                    // read data.voteAverage
                    dataVoteAverage = data.getVoteAverage();
                    // read data.popularity
                    dataPopularity = data.getPopularity();
                    // read data.originalTitle
                    dataOriginalTitle = data.getOriginalTitle();
                }


                // read String.valueOf(data.voteAverage)
                stringValueOfDataVoteAverage = java.lang.String.valueOf(dataVoteAverage);
                // read String.valueOf(data.popularity)
                stringValueOfDataPopularity = java.lang.String.valueOf(dataPopularity);
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.moviePopularity, stringValueOfDataPopularity);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.movieTitle, dataOriginalTitle);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.movieVoteAverage, stringValueOfDataVoteAverage);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): data
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}
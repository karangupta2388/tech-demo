package com.example.tech_demo

import android.app.Application
import com.example.tech_demo.di.networkModule
import com.example.tech_demo.di.repoModule
import com.example.tech_demo.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class AppApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@AppApplication)
            modules(listOf(networkModule, viewModelModule, repoModule))
        }
    }
}
package com.example.tech_demo.data

class Result<T>(val data: T?, val networkState: NetworkState) {

    enum class NetworkState {
        LOADING,
        ERROR,
        SUCCESS
    }

}
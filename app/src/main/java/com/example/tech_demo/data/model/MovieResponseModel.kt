package com.example.tech_demo.data.model

import com.google.gson.annotations.SerializedName

data class MovieResponseModel(
    @SerializedName("poster_path")
    val posterPath: String,
    @SerializedName("original_title")
    val originalTitle: String,
    @SerializedName("popularity")
    val popularity: Double,
    @SerializedName("vote_average")
    val voteAverage: Double
)
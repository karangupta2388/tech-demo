package com.example.tech_demo.data.model

data class MoviesModel(
    val posterPath: String,
    val originalTitle: String,
    val popularity: Double,
    val voteAverage: Double
) : Model
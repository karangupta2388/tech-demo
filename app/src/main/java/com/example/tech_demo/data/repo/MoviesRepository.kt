package com.example.tech_demo.data.repo

import com.example.tech_demo.data.MoviesAPIService
import com.example.tech_demo.data.model.MoviesResponseModel
import io.reactivex.Single

open class MoviesRepository(private val moviesApiService: MoviesAPIService) {

    fun fetchMovies(): Single<MoviesResponseModel> = moviesApiService.fetchMovies()
}
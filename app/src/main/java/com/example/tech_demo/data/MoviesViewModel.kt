package com.example.tech_demo.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.tech_demo.data.model.MoviesModel
import com.example.tech_demo.data.repo.MoviesRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MoviesViewModel(private val moviesRepository: MoviesRepository) : BaseViewModel() {

    private val moviesLiveData: MutableLiveData<Result<List<MoviesModel>>> = MutableLiveData()

    fun fetchMovies(): LiveData<Result<List<MoviesModel>>> {
        moviesRepository.fetchMovies()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it ->
                it.results.map {
                    MoviesModel(
                        it.posterPath,
                        it.originalTitle,
                        it.popularity,
                        it.voteAverage
                    )
                }.let {
                    Result(it, Result.NetworkState.SUCCESS)
                }
            }
            .subscribe(
                { moviesLiveData.postValue(it) },
                { moviesLiveData.postValue(Result(null, Result.NetworkState.ERROR)) })
            .disposeWhenCleared()

        moviesLiveData.value = Result(null, Result.NetworkState.LOADING)
        return moviesLiveData
    }
}

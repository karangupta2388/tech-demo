package com.example.tech_demo.data.model

import com.google.gson.annotations.SerializedName

data class MoviesResponseModel(
    @SerializedName("results")
    val results: List<MovieResponseModel>
)
package com.example.tech_demo.data

import com.example.tech_demo.data.model.MoviesResponseModel
import io.reactivex.Single
import retrofit2.http.GET

interface MoviesAPIService {

    @GET("fetchMovies/")
    fun fetchMovies() : Single<MoviesResponseModel>
}
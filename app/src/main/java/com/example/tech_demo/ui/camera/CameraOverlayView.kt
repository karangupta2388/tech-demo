package com.example.tech_demo

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat

class CameraOverlayView : View {

    private val paint = Paint()
    private val rectF = RectF()

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        paint.color = ContextCompat.getColor(context!!, R.color.white)
        paint.isAntiAlias = true
        paint.style = Paint.Style.STROKE
        //   paint.setStrokeWidth(4);
        paint.pathEffect = DashPathEffect( floatArrayOf(5f, 10f), 0f);
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        rectF.left = measuredWidth / 6f;
        rectF.top = measuredHeight * 0.15f
        rectF.right = 5f * measuredWidth / 6f
        rectF.bottom = measuredHeight * 0.65f
        canvas.drawOval(rectF, paint);
    }
}
package com.example.tech_demo.ui

import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

fun View.setThrottledClickListener(
    throttlePeriod: Long,
    onNext: Consumer<View>
): Disposable {
    val publishSubject = PublishSubject.create<View>()
    setOnClickListener {
        publishSubject.onNext(it)
    }

    return publishSubject.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .throttleFirst(throttlePeriod, TimeUnit.MILLISECONDS).subscribe(onNext)
}


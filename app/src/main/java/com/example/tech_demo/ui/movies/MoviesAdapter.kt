package com.example.tech_demo.ui.movies

import androidx.databinding.ViewDataBinding
import com.example.tech_demo.R

class MoviesAdapter() : BaseAdapter() {

    override fun getViewHolder(binding: ViewDataBinding): ViewHolderPresenter =
        MoviesViewHolderPresenter(binding)

    override fun getLayoutId(): Int = R.layout.movie_result
} 
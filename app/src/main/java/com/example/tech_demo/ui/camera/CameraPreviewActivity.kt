package com.example.tech_demo.ui.camera

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.example.tech_demo.R
import com.google.common.util.concurrent.ListenableFuture
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.*
import org.koin.core.component.KoinApiExtension

@KoinApiExtension
class CameraPreviewActivity : AppCompatActivity(),
    ImageAnalysis.Analyzer, View.OnClickListener {

    private var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>? = null
    private var previewView: PreviewView? = null
    private var previewImageContainer: FrameLayout? = null
    private var previewImage: ImageView? = null
    private var imageCapture: ImageCapture? = null
    private var videoCapture: VideoCapture? = null
    private var bRecord: Button? = null
    private var bCapture: Button? = null
    private var retry: Button? = null
    private var ok: Button? = null
    private var uri: Uri? = null
    private var bitmap: Bitmap? = null
    private var compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.camera_preview_activity)
        previewView = findViewById(R.id.previewView)
        previewImageContainer = findViewById(R.id.preview_image_container)
        previewImage = findViewById(R.id.preview_image)
        retry = findViewById(R.id.retry)
        retry!!.setOnClickListener { onBackPressed() }
        ok = findViewById(R.id.ok)
        ok!!.setOnClickListener {
            var file: File? = null
            val disposable: Disposable = Single
                .just(mediaStorageDir!!.path + File.separator + "IMG_" + "demo" + ".jpg")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { path_external ->
                    val bytes = ByteArrayOutputStream()
                    bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                    file = File(path_external)
                    try {
                        val fo = FileOutputStream(file)
                        fo.write(bytes.toByteArray())
                        fo.flush()
                        fo.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }.subscribe(
                    {
                        setResult(11, Intent().setData(Uri.fromFile(file)))
                        finish()
                    },
                    {
                        print("File from bitmap failed")
                    }
                )
            compositeDisposable.add(disposable)
        }
        bCapture = findViewById(R.id.bCapture)
        bRecord = findViewById(R.id.bRecord)
        bRecord!!.text = "start recording" // Set the initial text of the button
        bCapture!!.setOnClickListener(this)
        bRecord!!.setOnClickListener(this)
        cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture!!.addListener({
            try {
                val cameraProvider = cameraProviderFuture!!.get()
                startCameraX(cameraProvider)
            } catch (e: ExecutionException) {
                e.printStackTrace()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }, getExecutor())
    }

    private fun getExecutor(): Executor {
        return ContextCompat.getMainExecutor(this)
    }

    @SuppressLint("RestrictedApi")
    fun startCameraX(cameraProvider: ProcessCameraProvider) {
        cameraProvider.unbindAll()
        val cameraSelector = CameraSelector.Builder()
            .requireLensFacing(CameraSelector.LENS_FACING_FRONT)
            .build()
        val preview = Preview.Builder()
            .build()
        preview.setSurfaceProvider(previewView!!.surfaceProvider)

        // Image capture use case
        imageCapture = ImageCapture.Builder()
            .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
            .build()

        // Video capture use case
        videoCapture = VideoCapture.Builder()
            .setVideoFrameRate(30)
            .build()

        // Image analysis use case
        val imageAnalysis = ImageAnalysis.Builder()
            .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
            .build()
        imageAnalysis.setAnalyzer(getExecutor(), this)

        //bind to lifecycle:
        cameraProvider.bindToLifecycle(
            (this as LifecycleOwner?)!!,
            cameraSelector,
            preview,
            imageCapture,
            videoCapture
        )
    }

    override fun analyze(image: ImageProxy) {
        // Image processing here for the current frame
        Log.d("TAG", "analyze: got the frame at: " + image.imageInfo.timestamp)
        image.close()
    }

    @SuppressLint("RestrictedApi")
    override fun onClick(view: View) {
        when (view.id) {
            R.id.bCapture -> capturePhoto()
            R.id.bRecord -> if (bRecord!!.text === "start recording") {
                bRecord!!.text = "stop recording"
                recordVideo()
            } else {
                bRecord!!.text = "start recording"
                videoCapture!!.stopRecording()
            }
        }
    }

    @SuppressLint("RestrictedApi")
    fun recordVideo() {
        if (videoCapture != null) {
            val movieDir = File("/mnt/sdcard/Movies/CameraXMovies")
            if (!movieDir.exists()) movieDir.mkdir()
            val date = Date()
            val timestamp = date.time.toString()
            val vidFilePath = movieDir.absolutePath + "/" + timestamp + ".mp4"
            val vidFile = File(vidFilePath)
            try {
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.RECORD_AUDIO
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return
                }
                videoCapture!!.startRecording(
                    VideoCapture.OutputFileOptions.Builder(vidFile).build(),
                    getExecutor(),
                    object : VideoCapture.OnVideoSavedCallback {
                        override fun onVideoSaved(outputFileResults: VideoCapture.OutputFileResults) {
                            Toast.makeText(
                                this@CameraPreviewActivity,
                                "Video has been saved successfully.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        override fun onError(
                            videoCaptureError: Int,
                            message: String,
                            cause: Throwable?
                        ) {
                            Toast.makeText(
                                this@CameraPreviewActivity,
                                "Error saving video: $message",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun capturePhoto() {
        val imageFilePath: File = createImageFilePath(0) ?: return
        imageCapture!!.takePicture(
            ImageCapture.OutputFileOptions.Builder(imageFilePath).build(),
            getExecutor(),
            object : ImageCapture.OnImageSavedCallback {
                override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
                    Toast.makeText(
                        this@CameraPreviewActivity,
                        "Photo has been saved successfully.",
                        Toast.LENGTH_SHORT
                    ).show()
                    //   previewImage.setImageURI(imageFilePath));
                    val bitmap = BitmapFactory.decodeFile(imageFilePath.absolutePath)
                    this@CameraPreviewActivity.bitmap =
                        rotateImage(imageFilePath.absolutePath, bitmap)
                    //  previewImage.setImageURI(Uri.fromFile(imageFilePath));
                    previewImageContainer!!.visibility = View.VISIBLE
                    //  previewImage.setScaleX(-2);
                    //   previewImage.setScaleY(-2);
                    previewImage!!.setImageBitmap(this@CameraPreviewActivity.bitmap)
                    getImageSize(imageFilePath)
                }

                override fun onError(exception: ImageCaptureException) {
                    Toast.makeText(
                        this@CameraPreviewActivity,
                        "Error saving photo: " + exception.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        )
    }

    fun getImageSize(imageFilePath: File): IntArray? {
        return try {
            uri = Uri.fromFile(imageFilePath)
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = true
            val input: InputStream? = this.contentResolver.openInputStream(uri!!)
            BitmapFactory.decodeStream(input, null, options)
            input?.close()
            intArrayOf(options.outHeight, options.outWidth)
        } catch (e: Exception) {
            intArrayOf(0, 0)
        }
    }

    private fun rotateImage(photoPath: String, bitmap: Bitmap): Bitmap? {
        var ei: ExifInterface? = null
        try {
            ei = ExifInterface(photoPath)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        val orientation = ei!!.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )
        val rotatedBitmap: Bitmap? = when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(bitmap, 90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(bitmap, 180f)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(bitmap, 270f)
            ExifInterface.ORIENTATION_NORMAL -> rotateImage(bitmap, 0f)
            else -> rotateImage(bitmap, 0f)
        }
        return rotatedBitmap
    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postScale(-1f, 1f)
        matrix.postRotate(if (angle == 90f || angle == 270f) angle + 180 else angle)
        return Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
    }

    override fun onBackPressed() {
        if (previewImageContainer!!.visibility == View.VISIBLE) {
            previewImageContainer!!.visibility = View.GONE
            ThreadPoolExecutor(
                5, 128, 1,
                TimeUnit.SECONDS, LinkedBlockingQueue()
            )
            return
        }
        super.onBackPressed()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    var mediaStorageDir: File? = null

    private fun createImageFilePath(extraTimeStamp: Int): File? {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            mediaStorageDir =
                File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "/TechDemoCamera")
        } else {
            mediaStorageDir = File(Environment.getExternalStorageDirectory(), "/TechDemoCamera")
        }
        if (!mediaStorageDir!!.exists()) {
            if (!mediaStorageDir!!.mkdirs()) {
                Toast.makeText(
                    this@CameraPreviewActivity,
                    "failed to create TechDemoCamera directory",
                    Toast.LENGTH_SHORT
                ).show()
                return null
            }
        }
        var timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
        if (extraTimeStamp > 0) {
            timeStamp += extraTimeStamp
        }
        return File(mediaStorageDir!!.path + File.separator + "IMG_" + "demo" + ".jpg")
    }
}

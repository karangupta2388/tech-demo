package com.example.tech_demo.ui.main

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.tech_demo.R
import com.example.tech_demo.ui.camera.CameraActivity
import com.example.tech_demo.ui.movies.MoviesActivity
import org.koin.core.component.KoinApiExtension

@KoinApiExtension
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        findViewById<TextView>(R.id.movies).setOnClickListener {
            startActivity(Intent(this, MoviesActivity::class.java))
        }
        findViewById<TextView>(R.id.click_selfie).setOnClickListener {
            startActivity(Intent(this, CameraActivity::class.java))
        }
    }
}
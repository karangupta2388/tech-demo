package com.example.tech_demo.ui.movies

import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.tech_demo.data.model.Model
import com.example.tech_demo.data.model.MoviesModel
import com.example.tech_demo.databinding.MovieResultBinding

class MoviesViewHolderPresenter(binding: ViewDataBinding) : ViewHolderPresenter(binding) {

    override fun bind(data: Model) {
        if (data !is MoviesModel || binding !is MovieResultBinding) {
            return
        }
        Glide.with(binding.root.context).load(data.posterPath)
            .apply(RequestOptions.circleCropTransform()).into(binding.moviePoster)
    }
}
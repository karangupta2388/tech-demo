package com.example.tech_demo.ui.camera

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult
import androidx.appcompat.app.AppCompatActivity
import com.example.tech_demo.R
import org.koin.core.component.KoinApiExtension

@KoinApiExtension
class CameraActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.primary_view)
        registerForActivityResult(
            StartActivityForResult()
        ) { result: ActivityResult ->
            if (result.resultCode == RESULT_OK && result.data != null) {
                val resultImage: ImageView = findViewById(R.id.result_image)
                resultImage.visibility = View.VISIBLE
                resultImage.setImageURI(result.data?.data)
            }
        }.launch(Intent(this, CameraPreviewActivity::class.java))
    }
}
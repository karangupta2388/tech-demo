package com.example.tech_demo.ui.movies

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.example.tech_demo.R
import com.example.tech_demo.data.MoviesViewModel
import com.example.tech_demo.data.Result
import com.example.tech_demo.ui.setThrottledClickListener
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.movies_activity.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.component.KoinApiExtension

private const val DEFAULT_THROTTLE_PERIOD = 500.toLong()

@KoinApiExtension
class MoviesActivity : BaseActivity() {
    private val moviesViewModel: MoviesViewModel by viewModel()
    private val moviesAdapter = MoviesAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.movies_activity)
        movies_list.adapter = moviesAdapter
        network_retry.setThrottledClickListener(DEFAULT_THROTTLE_PERIOD, Consumer { fetchMovies() })
            .clearOnDestroy()
        fetchMovies()
    }

    private fun fetchMovies() {
        moviesViewModel.fetchMovies().observe(this, Observer {
            if (it.networkState == Result.NetworkState.SUCCESS) {
                if (it.data != null) {
                    movies_list.visibility = View.VISIBLE
                    network_retry.visibility = View.GONE
                    spinner.visibility = View.GONE
                    moviesAdapter.setValues(it.data)
                }
            } else if (it.networkState == Result.NetworkState.LOADING) {
                network_retry.visibility = View.GONE
                spinner.visibility = View.VISIBLE
            } else if (it.networkState == Result.NetworkState.ERROR) {
                movies_list.visibility = View.GONE
                spinner.visibility = View.GONE
                network_retry.visibility = View.VISIBLE
            }
        })
    }
}
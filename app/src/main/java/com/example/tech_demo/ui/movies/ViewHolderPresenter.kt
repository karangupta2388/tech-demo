package com.example.tech_demo.ui.movies

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.tech_demo.data.model.Model

abstract class ViewHolderPresenter(val binding: ViewDataBinding): RecyclerView.ViewHolder(binding.root) {
    abstract fun bind(data: Model)
}
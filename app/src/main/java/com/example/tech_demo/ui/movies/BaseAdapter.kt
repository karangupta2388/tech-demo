package com.example.tech_demo.ui.movies

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.example.tech_demo.data.model.Model

abstract class BaseAdapter :
    RecyclerView.Adapter<ViewHolderPresenter>() {
    private var dataList: List<Model> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderPresenter {
        return getViewHolder(
            DataBindingUtil.inflate<ViewDataBinding>(
                LayoutInflater.from(parent.context),
                getLayoutId(),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolderPresenter, position: Int) {
        val data = dataList[position]
        holder.binding.setVariable(BR.data, data)
        holder.bind(data)
        holder.binding.executePendingBindings()
    }

    fun setValues(dataList: List<Model>) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = dataList.size

    abstract fun getViewHolder(binding: ViewDataBinding): ViewHolderPresenter

    abstract fun getLayoutId(): Int
}
package com.example.tech_demo.di

import com.example.tech_demo.data.repo.MoviesRepository
import org.koin.dsl.module

val repoModule = module {
    factory { MoviesRepository(get()) }
}
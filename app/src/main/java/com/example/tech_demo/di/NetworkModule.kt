package com.example.tech_demo.di

import com.example.tech_demo.data.MoviesAPIService
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

const val BASE_URL = "http://www.interviewMovies.api/"

val networkModule = module {
    single {
        OkHttpClient()
    }

    single<CallAdapter.Factory> { RxJava2CallAdapterFactory.create() }

    single {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(get())
            .client(get<OkHttpClient>()).build();
    }

    factory {
        get<Retrofit>().create(MoviesAPIService::class.java)
    }
}

package com.example.tech_demo.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.tech_demo.data.model.MovieResponseModel
import com.example.tech_demo.data.model.MoviesResponseModel
import com.example.tech_demo.data.repo.MoviesRepository
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class MoviesViewModelUnitTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    @Test
    fun assertFetchMovies() {
        // Setting up mocks
        val moviesApiService = mock<MoviesAPIService>()
        val moviesRepository = MoviesRepository(moviesApiService)
        val moviesViewModel = MoviesViewModel(moviesRepository)

        val single = Single.just(
            MoviesResponseModel(
                listOf(MovieResponseModel("Mock Patch", "Mock Title", 8.9, 9.0))
            )
        )

        // Given: The viewModel tries to fetch a movie
        whenever(moviesApiService.fetchMovies()).thenReturn(single)

        // Then: Then the repository returns the correct list of movies
        val result = moviesViewModel.fetchMovies().value!!
        assertEquals(result.networkState, Result.NetworkState.SUCCESS)
    }
}